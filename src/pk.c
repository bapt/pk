/*-
 * Copyright (c) 2021 Baptiste Daroussin <bapt@FreeBSD.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer
 *    in this position and unchanged.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef HAVE_CONFIG_H
#include "pk_config.h"
#endif

#include <sys/param.h>

#include <sys/stat.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sysctl.h>
#include <sys/user.h>
#include <sys/proc.h>
#include <sys/mman.h>
#include <sys/wait.h>

#include <assert.h>
#include <ctype.h>
#include <err.h>
#include <fcntl.h>
#include <errno.h>
#include <lauxlib.h>
#include <lualib.h>
#include <lfs.h>
#include <lua_ucl.h>
#include <getopt.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <spawn.h>
#include <signal.h>

#include "pk.h"

extern char **environ;

#define luaL_dobuffer(L, s, sz) \
	(luaL_loadbuffer(L, s, sz, s) || lua_pcall(L, 0, LUA_MULTRET, 0))

struct pk_ctx {
	int cmdfd;
	char cmdpath[MAXPATHLEN];
	char libpath[MAXPATHLEN];
};

/* Used to define why do we show usage message to a user */
enum pkg_usage_reason {
	PKG_USAGE_ERROR,
	PKG_USAGE_UNKNOWN_COMMAND,
	PKG_USAGE_INVALID_ARGUMENTS,
	PKG_USAGE_HELP
};

static void usage(void);
static int exec_help(int, char **);

static int
exec_version(int argc __unused, char **argv __unused)
{
	printf(PK_VERSION""GITHASH"\n");
	exit(EXIT_SUCCESS);
	/* NOTREACHED */
}

static struct commands {
	const char * const name;
	const char * const desc;
	int (*exec)(int argc, char **argv);
	void (* const usage)(void);
} cmd[] = {
	{ "bulk", "Generate packages for given ports", NULL, NULL },
	{ "distclean", "Remove old distfiles", NULL, NULL },
	{ "daemon", "Launch the poudriere daemon", NULL, NULL },
	{ "help", "Show usage", &exec_help, NULL },
	{ "image", "Generate images", NULL, NULL },
	{ "jail", "Manage the jails used by pk", NULL, NULL },
	{ "logclean", "Remove old logfiles", NULL, NULL },
	{ "ports", "Create, update or delete the portstrees used by pk", NULL, NULL},
	{ "pkgclean", "Remove packages that are no longer needed", NULL, NULL },
	{ "queue", "Queue a build request", NULL, NULL },
	{ "status", "Get the status of builds", NULL, NULL },
	{ "testport", "Launch a test build of a given port", NULL, NULL },
	{ "version", "Show the version of pk", &exec_version, NULL },
};

#define NELEM(array)    (sizeof(array) / sizeof((array)[0]))
static const unsigned int cmd_len = NELEM(cmd);

static void
usage(void)
{
	fprintf(stderr, "usage\n");
	exit(EXIT_FAILURE);
}

static int
exec_help(int argc, char **argv)
{
	char *manpage;
	unsigned int i;

	if ((argc != 2) || (strcmp("help", argv[1]) == 0)) {
		usage();
		return(EXIT_FAILURE);
	}

	for (i = 0; i < cmd_len; i++) {
		if (strcmp(cmd[i].name, argv[1]) == 0) {
			if (asprintf(&manpage, "/usr/bin/man pkg-%s", cmd[i].name) == -1)
				errx(EXIT_FAILURE, "cannot allocate memory");

			system(manpage);
			free(manpage);

			return (0);
		}
	}
	if (strcmp(argv[1], "pk") == 0) {
		system("/usr/bin/man 8 pk");
		return (0);
	} else if (strcmp(argv[1], "pk.conf") == 0) {
		system("/usr/bin/man 5 pk.conf");
		return (0);
	}

	/* Command name not found */
	warnx("'%s' is not a valid command.\n", argv[1]);

	fprintf(stderr, "See 'pk help' for more information on the commands.\n");

	return (EXIT_FAILURE);
}

static const char **
luaL_checkarraystrings(lua_State *L, int arg)
{
	const char **ret;
	lua_Integer n, i;
	int t;
	int abs_arg = lua_absindex(L, arg);
	luaL_checktype(L, abs_arg, LUA_TTABLE);
	n = lua_rawlen(L, abs_arg);
	ret = lua_newuserdata(L, (n+1)*sizeof(char *));
	for (i = 0; i < n; i++) {
		t = lua_rawgeti(L, abs_arg, i+1);
		if (t == LUA_TNIL)
			break;
		luaL_argcheck(L, t == LUA_TSTRING, arg, "expected array of strings");
		ret[i] = lua_tostring(L, -1);
		lua_pop(L, 1);
	}
	ret[i] = NULL;
	return ret;
}

static int
lua_exec(lua_State *L)
{
	int r, pstat;
	posix_spawn_file_actions_t action;
	int stdin_pipe[2] = { -1, -1 };
	pid_t pid;
	const char **argv;
	int n = lua_gettop(L);
	luaL_argcheck(L, n == 1, n > 1 ? 2 : n,
	    "pk.exec takes exactly one argument");

	if (pipe(stdin_pipe) < 0)
		return 3;

	posix_spawn_file_actions_init(&action);
	posix_spawn_file_actions_adddup2(&action, stdin_pipe[0], STDIN_FILENO);
	posix_spawn_file_actions_addclose(&action, stdin_pipe[1]);

	argv = luaL_checkarraystrings(L,1);
	if (0 != (r = posix_spawnp(&pid, argv[0], &action, NULL,
		(char *const *)argv, environ))) {
		lua_pushnil(L);
		lua_pushstring(L, strerror(r));
		lua_pushinteger(L, r);
		return 3;
	}
	while (waitpid(pid, &pstat, 0) == -1) {
		if (errno != EINTR) {
			lua_pushnil(L);
			lua_pushstring(L, strerror(r));
			lua_pushinteger(L, r);
			return (3);
		}
	}
	if (WEXITSTATUS(pstat) != 0) {
		lua_pushnil(L);
		lua_pushstring(L, "Abnormal termination");
		lua_pushinteger(L, r);
		return (3);
	}
	posix_spawn_file_actions_destroy(&action);

	if (stdin_pipe[0] != -1)
		close(stdin_pipe[0]);
	if (stdin_pipe[1] != -1)
		close(stdin_pipe[1]);
	lua_pushinteger(L, pid);
	return 1;
}

int
dolua(struct pk_ctx *ctx, struct commands *cmd, int argc, char **argv)
{
	struct stat st;
	char *buf;
	lua_State *L;

	if (fstatat(ctx->cmdfd, cmd->name, &st, 0) == -1) {
		return (ENOENT);
	}

	int fd = openat(ctx->cmdfd, cmd->name, O_RDONLY);
	if (fd == -1)
		err(EXIT_FAILURE, "openat");

	buf = mmap(NULL, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
	close(fd);

	if (buf == NULL)
		return (EINVAL);

	int ret = 1;
	L = luaL_newstate();
	luaL_openlibs(L);
	luaL_requiref(L, "lfs", luaopen_lfs, 1);
	luaL_requiref(L, "ucl", luaopen_ucl, 1);
	lua_getglobal( L, "package" );
	lua_pushstring( L, ctx->libpath );
	lua_setfield( L, -2, "path" );
	lua_pop( L, 1 );

	static const luaL_Reg pk_lib[] = {
		{ "exec", lua_exec },
		{ NULL, NULL },
	};
	luaL_newlib(L, pk_lib);
	lua_setglobal(L, "pk");
	/* stolen from lua.c */
	lua_createtable(L, --argc, 1);
	argv++;
	for (int i = 0; i < argc; i++) {
		lua_pushstring(L, argv[i]);
		/* lua starts counting by 1 */
		lua_rawseti(L, -2, i + 1);
	}
	lua_setglobal(L, "arg");
	lua_pushstring(L, PREFIX);
	lua_setglobal(L, "prefix");
	if (luaL_dobuffer(L, buf, st.st_size)) {
		warnx("fail to execute lua %s", lua_tostring(L, -1));
		goto out;
	}

	ret = 0;
out:
	munmap(buf, st.st_size);

	return (ret);
}

int
main(int argc, char **argv)
{
	unsigned int	  i;
	struct commands	 *command = NULL;
	unsigned int	  ambiguous = -1;
	size_t		  len;
	int		  ret = EXIT_SUCCESS;
	struct pk_ctx	 ctx;

	/* Set stdout unbuffered */
	setvbuf(stdout, NULL, _IONBF, 0);

	if (argc < 2)
		usage();

	umask(022);

	int mib[4];
	mib[0] = CTL_KERN;
	mib[1] = KERN_PROC;
	mib[2] = KERN_PROC_PATHNAME;
	mib[3] = -1;
	char whereami[MAXPATHLEN];
	char buf[MAXPATHLEN];
	size_t cb = sizeof(buf);
	sysctl(mib, 4, whereami, &cb, NULL, 0);
	char *dir=dirname(whereami);
	snprintf(buf, MAXPATHLEN, "%s/../share/pk/commands", dir);
	if (realpath(buf, ctx.cmdpath) == NULL)
		errx(EXIT_FAILURE, "enable to find the command path: %s",
		    buf);
	snprintf(buf, MAXPATHLEN, "%s/../share/pk/libs", dir);
	if (realpath(buf, ctx.libpath) == NULL)
		errx(EXIT_FAILURE, "enable to find the lib path: %s",
		    buf);
	strlcat(ctx.libpath, "/?.lua", MAXPATHLEN);

	ctx.cmdfd = open(ctx.cmdpath, O_DIRECTORY);
	if (ctx.cmdfd == -1)
		err(EXIT_FAILURE, "open(%s)", buf);

	len = strlen(argv[1]);
	for (i = 0; i < cmd_len; i++) {
		if (strncmp(argv[1], cmd[i].name, len) == 0) {
			/* if we have the exact cmd */
			if (len == strlen(cmd[i].name)) {
				command = &cmd[i];
				ambiguous = 0;
				break;
			}

			/*
			 * we already found a partial match so `argv[0]' is
			 * an ambiguous shortcut
			 */
			ambiguous++;

			command = &cmd[i];
		}
	}

	if (ambiguous == 0) {
		if (command->exec == NULL) {
			ret = dolua(&ctx, command, --argc, ++argv);
		} else {
			ret = command->exec(--argc, ++argv);
		}
	} else {
		usage();
	}

	close(ctx.cmdfd);
	return (EXIT_SUCCESS);
}

