-- Copyright (c) 2021-2024 Baptiste Daroussin <bapt@FreeBSD.org>
--
-- SPDX-License-Identifier: BSD-2-Clause

local jails = {}
local pklib = require("pklib")

function jails.list()
	local jail = {}
	local dir = prefix .. "/etc/poudriere.d/jails"
	for file in lfs.dir(dir) do
		if file ~= "." and file ~= ".." then
			local j = jails.get(file)
			table.insert(jail, j)
		end
	end
	return jail
end

function jails.get(name)
	if not name then return nil end
	local dir = prefix .. "/etc/poudriere.d/jails/" .. name
	local attr = lfs.attributes(dir)
	if not attr or attr.mode ~= "directory" then
		return nil
	end
	local j = {}
	j["name"] = name
	j["mnt"] = pklib.fcat(dir .. "/mnt")
	j["method"] = pklib.fcat(dir .. "/method")
	j["arch"] = pklib.fcat(dir .. "/arch")
	j["fs"] = pklib.fcat(dir .. "/fs")
	j["pkgbase"] = pklib.fcat(dir .. "/pkgbase")
	j["timestamp"] = pklib.fcat(dir .. "/timestamp")
	j["version"] = pklib.fcat(dir .. "/version")
	return j
end

return jails
