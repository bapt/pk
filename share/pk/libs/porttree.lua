-- Copyright (c) 2021-2024 Baptiste Daroussin <bapt@FreeBSD.org>
--
-- SPDX-License-Identifier: BSD-2-Clause

local porttree = {}
local pklib = require("pklib")

function porttree.list()
	local pt = {}
	local dir = prefix .. "/etc/poudriere.d/ports/"
	for file in lfs.dir(dir) do
		if file ~= "." and file ~= ".." then
			local _pt = porttree.get(file)
			table.insert(pt, _pt)
		end
	end
	return pt
end

function porttree.get(name)
	if not name then return nil end
	local dir = prefix .. "/etc/poudriere.d/ports/" .. name
	local attr = lfs.attributes(dir)
	if not attr or attr.mode ~= "directory" then
		return nil
	end
	local pt = {}
	pt["mnt"] = pklib.fcat(dir .. "/mnt", "r")
	pt["method"] = pklib.fcat(dir .. "/method", "r")
	pt["timestamp"] = pklib.fcat(dir .. "/timestamp", "r")
	return pt
end

function porttree.update(pt)
	if not pt.method then
		pklib.err("Don't know how to update " ..pt.name .. "\n" )
	end
	local m = pklib.fetch_method[pt.method]
	if m == nil then
		pklib.err("Don't know how to update " ..pt.name .. "\n" )
	end
	m.update(pt.path)
	local dir = prefix .. "/etc/poudriere.d/ports/" .. pt.name
	local f = assert(io.open(dir .. "/timestamp", "w"))
	f:write(os.time())
	f:close()
end

function porttree.create(pt)
end

return porttree
