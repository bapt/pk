-- Copyright (c) 2024 Baptiste Daroussin <bapt@FreeBSD.org>
--
-- SPDX-License-Identifier: BSD-2-Clause

local pklib = {}
local lfs = require("lfs")

local git = {}
function git.update(path)
	local pwd = lfs.currentdir()
	lfs.chdir(path)
	pk.exec({"git","pull","-r", "-q"})
	lfs.chdir(pwd)
end

function git.create()
end

function git.delete()
end
local fetch_method = {}
fetch_method["git+https"] = git
fetch_method["git+http"] = git
fetch_method["git+ssh"] = git

function pklib.err(arg)
	io.stderr:write(arg)
	os.exit(1)
end

function pklib.warn(arg)
	io.stderr:write(arg)
end

function pklib.fcat(path)
	local f1 = assert(io.open(path, "r"))
	local val = nil
	if f1 then
		val = f1:read()
		f1:close()
	end
	return val
end

function pklib.set_or_die(oldval, newval, func)
	if oldval then
		func()
	end
	return newval
end


pklib.fetch_method = fetch_method
return pklib
